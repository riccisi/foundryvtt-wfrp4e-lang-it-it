{
	"label": "Psicologia",
	"entries": [
		{
			"id": "Animosity (Target)",
			"name": "Animosità (Bersaglio)",
			"description": "<p>Il Personaggio nutre ostilità verso il Bersaglio, di solito un gruppo, come ‘Nordlander’, ‘Uominibestia’ o ‘Nobili’. Quando lo incontra deve effettuare una Prova di Psicologia. Se ha successo riesce a controllarsi e subisce solo -20 alle Prove di Socialità rispetto al gruppo. In caso di fallimento è vittima dell’ <em>Animosità </em>.</p>\n<p>Alla fine di ogni Round successivo potrà tentare un’ulteriore Prova per controllarsi. Anche se non riesce gli effetti svaniscono naturalmente appena tutti i Bersagli visibili sono pacificati o se ne vanno, oppure se il Personaggio diviene <em>Stordito , Privo di Sensi</em> o soggetto a un altro Tratto Psicologico.</p>\n<p>Mentre è vittima di <em>Animosità </em>, il Personaggio deve attaccare il Bersaglio verbalmente (insulti e freddure) o fisicamente (di solito a pugni), come appropriato. Inoltre, ottiene +1 LS a queste aggressioni. <em>Paura e Terrore</em> prevalgono su <em>Animosità</em> .</p>"
		},
		{
			"id": "Camaraderie (Group)",
			"name": "Cameratismo (Gruppo)",
			"description": "<p><em>Cameratismo</em> riflette sentimenti positivi verso un gruppo. Quando questo viene minacciato, fisicamente o socialmente, il Personaggio dovrà correre in soccorso e otterrà +1 LS alle Prove aiutare o supportare il gruppo.</li></ul>\n<p><strong>Esempio:</strong> <em>Amhold ha avuto un’ infanzia difficile, un bambino di strada senza una famiglia. Nonostante il carattere ombroso, ha </em>Cameratismo (Orfani)<em>.</em></p>"
		},
		{
			"id": "Fear (Rating)",
			"name": "Paura (Grado)",
			"description": "<p><em>Paura</em> rappresenta un’estrema avversione per qualcosa. Il Grado di Paura rappresenta il LS necessario per superarla. Per farlo si effettua una <strong>Prova Prolungata di Freddezza</strong>, tirando alla fine di ogni Round. Fintanto che non viene superata con successo, si è soggetti a <em>Paura</em>.</p>\n<p>Chi è vittima di <em>Paura</em> subisce -1 LS in tutte le Prove legate alla fonte. Non ci si può avvicinare ad essa senza superare una <strong>Prova Impegnativa (+0) di Freddezza</strong>. Se, invece, è la fonte ad avvicinarsi, è necessario superare una <strong>Prova Impegnativa (+0) di Freddezza</strong> per non subire 1 <em>Atterrito</em>.</li></ul>"
		},
		{
			"id": "Frenzy",
			"name": "Furia",
			"description": "<p>Superando una Prova di Volontà, il Personaggio può caricarsi di rabbia, ululando, mordendo lo scudo o in altri modi. Se ha successo diviene soggetto a <em>Furia</em>.</p>\n<p>Mentre è in <em>Furia</em> il Personaggio diventa immune a tutti gli altri Tratti Psicologici e non fuggirà né si ritirerà per alcun motivo, anzi dovrà sempre muoversi alla massima velocità per attaccare l'avversario più vicino. In genere, le uniche Azioni che può compiere sono Prove di Abilità di Combattimento o Atletica, per raggiungerlo più in fretta. </p>\n<p>Nel corso di ogni Round potrà compiere una Prova di Mischia gratuita, dato che sta attaccando senza risparmiarsi. Infine, ottiene +1 Bonus Forza grazie a tanta ferocia. Rimane in <em>Furia</em> finché tutti i nemici visibili non sono resi innocui o finché non subisce la Condizione <em>Stordito</em> o <em>Privo di Sensi</em> . Al termine della Furia subisce 1 <em>Affaticato</em>. </p>"
		},
		{
			"id": "Hatred (Target)",
			"name": "Odio (Bersaglio)",
			"description": "<p>Il Personaggio è consumato dall’ <em>Odio</em> verso il <em>Bersaglio</em>, di solito un gruppo, come ‘Hochlander’, ‘Piovre di Palude’ o ‘Schiavisti’, al punto che non potrà mai interagirvi in modo pacifico. Quando lo incontra deve superare una Prova di Psicologia per trattenersi. Se fallisce sarà soggetto all’ <em>Odio</em>, e alla fine di ogni Round seguente può ritentare la Prova per porvi un freno. Gli effetti hanno termine appena tutti i <em>Bersagli</em> visibili sono morti o fuggiti, oppure se cade <em>Privo di Sensi</em>. Mentre è vittima dell’ <em>Odio</em>, il Personaggio è costretto ad aggredire il <em>Bersaglio</em> senza risparmiarsi. Ottiene +1 LS in tutte le Prove di combattimento contro di esso ed è immune a <em>Paura</em> o Intimidire provocati da questi nemici, ma non al <em>Terrore</em>.</li></ul>"
		},
		{
			"id": "Phobia (Target)",
			"name": "Fobia (Bersaglio)",
			"description": "<p>La <em>Fobia</em> riflette una paura specifica, come quella nei confronti di una particolare creatura, oggetto o circostanza, come <em>Fobia (Insetti) , Fobia (Libri) o Fobia (Spazi Chiusi)</em> . L’oggetto della Fobia causa <em>Paura</em> 1 nel Personaggio e, nel caso di sindromi molto radicate, questo valore può aumentare. </li>\n</ul>\n<p><strong>Esempio:</strong> <em>Il Dottor Johannsen è un celebre antiquario. Nonostante le sue molte avventure soffre di </em>Fobia (Serpenti)<em> e nonostante ciò sembra incontrarli con tremenda regolarità.</em></p>"
		},
		{
			"id": "Prejudice",
			"name": "Pregiudizio (Bersaglio)",
			"description": "<p>Il Personaggio detesta il <em>Bersaglio</em>, di solito un gruppo, come ‘Ostlander’, ‘Elfi’ o ‘Maghi’. Quando lo incontra deve effettuare una Prova di Psicologia. Se la supera potrebbe accigliarsi, ma agirà in modo normale, subendo solo -10 alle Prove di Socialità relative. Se fallisce cede ai propri <em>Pregiudizi</em>. Alla fine di ogni Round seguente potrà tentare una prova per controllarsi. Se non lo fa, gli effetti hanno termine appena tutti i <em>Bersagli</em> visibili se ne sono andati, oppure se il Personaggio è <em>Stordito, Privo di Sensi</em> o soggetto a un altro Tratto Psicologico.</p>\n<p>&nbsp;</p>\n<p>Chi è vittima di <em>Pregiudizio</em> deve insultare il <em>Bersaglio</em>. Subito. Ad alta voce.</p>"
		},
		{
			"id": "Terror (Rating)",
			"name": "Terrore (Grado)",
			"description": "<p>Alcune creature sono talmente spaventose da provocare un profondo <em>Terrore</em> nei nemici. Chi ne incontra una deve effettuare una Prova di Freddezza. Se la supera non ci sarà alcun effetto, se fallisce subisce un numero di <em>Atterrito</em> pari al Grado di <em>Terrore</em> più il numero di LS sotto 0.</p>\n<p>Dopo aver risolto questa Prova di Psicologia, la creatura causerà <em>Paura</em> con Grado pari a quello di <em>Terrore</em>.</p>"
		},
		{
			"id": "Trauma",
			"name": "Trauma",
			"description": "<p>La vita di molti Reiklander è breve e brutale, ancor di più per gli avventurieri, che spesso subiscono dei <em>Traumi</em>. Ognuno, in base al proprio carattere, reagisce a un <em>Trauma</em> in modo diverso: incubi, abuso di sostanze, flashback, <em>Animosità</em> o <em>Odio</em> verso certi gruppi, oppure una <em>Fobia</em>. Si possono anche sviluppare effetti diversi nel corso del tempo. </p>\n<p>&nbsp;</p>\n<p><strong>Esempio:</strong> <em>Il villaggio dove viveva Horst fu distrutto da un terribile incendio e nei suoi sogni sente ancora le urla. Quando vede un altro Personaggio </em>In Fiamme<em> deve superare una <strong>Prova Impegnativa (+0) di Freddezza</strong>. Se fallisce subisce 1 </em>Stordito<em>, più uno </em>Stordito<em> per LS sotto 0. Inoltre, ogni notte deve superare una <strong>Prova Facile (+40) di Freddezza</strong> per non avere incubi e subire 1 </em>Affaticato.</p>"
		}
	]
}
